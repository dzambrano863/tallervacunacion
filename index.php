<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aplicacion Vacunacion</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body >
    <h1 style="text-align: center;">Formulario información de vacunación</h1>
    <div class="container"  >
        <form method="POST" action="">
            <div class="row">
                <div class="col-4">
                    <label for="exampleFormControlInput1" class="form-label">Identificacion</label>
                    <input type="text" class="form-control" id="identificacion" name="identificacion" required>
                </div>
                <div class="col-4">
                    <label for="exampleFormControlInput1" class="form-label">Nombres</label>
                    <input type="text" class="form-control" id="nombres" name="nombres" required>
                </div>
                <div class="col-4">
                    <label for="exampleFormControlInput1" class="form-label">Apellidos</label>
                    <input type="text" class="form-control" id="apellidos" name="apellidos" required>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <label for="exampleFormControlInput1" class="form-label">Tipo de biologico</label>
                    <input type="text" class="form-control" id="biologico" name="biologico" required>
                </div>
                <div class="col-4">
                    <label for="exampleFormControlInput1" class="form-label">Fecha primera dosis</label>
                    <input type="date" class="form-control" id="primera" name="primera" required>
                </div>
                <div class="col-4">
                    <label for="exampleFormControlInput1" class="form-label">Fecha segunda dosis</label>
                    <input type="date" class="form-control" id="segunda" name="segunda" required>
                </div>

            </div>
            <br>
            <input type="submit" value="Calcular" class="btn btn-primary" required >
        </form>

        <table class="table" style="   color: aliceblue!important;">
            <thead>
                <tr>

                    <th scope="col">Identificación</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Tipo de biologico</th>
                    <th scope="col">Fecha primera dosis</th>
                    <th scope="col">Fecha segunda dosis</th>
                </tr>
            </thead>
            <tbody>

                <?php
                

                function imprimir(){

                    if (file_exists("vacunas.txt")) {
                        $archivo = fopen("vacunas.txt", "r");
                        while (!feof($archivo)) {
                            $linea = fgets($archivo);
                            //  echo $linea."<br/>";     
                            $vacunados = explode(";", $linea);
                            echo " <tr>
                                            <th scope='row'>$vacunados[0]</th>
                                            <td>$vacunados[1]</td>
                                            <td>$vacunados[2]</td>
                                            <td>$vacunados[3]</td>
                                            <td>$vacunados[4]</td>
                                            <td>$vacunados[5]</td>
                                    </tr>";
    
                        }
                    }
                 
                 }

                if (isset($_POST['identificacion'])) {
                    if (file_exists("vacunas.txt")) {
                        $archivo = fopen("vacunas.txt", "r");
                        $pasa=True;
                        while (!feof($archivo)) {
                            $linea = fgets($archivo);
                            $vacunados = explode(";", $linea);
                            if($vacunados[0]===$_POST["identificacion"]){
                                $pasa=false;
                            }  
                            
                        }
                        if($pasa===True)
                        {
                            $contenido = $_POST["identificacion"] . ";" . $_POST["nombres"] . ";" . $_POST["apellidos"] . ";" . $_POST["biologico"] . ";" . $_POST["primera"] . ";" . $_POST["segunda"];
                            $archivo = fopen("vacunas.txt", "a");
                            fwrite($archivo, PHP_EOL . "$contenido");
                            fclose($archivo);
                        }
                      
                        $pasa=True;
                    } else {
                        $contenido = $_POST["identificacion"] . ";" . $_POST["nombres"] . ";" . $_POST["apellidos"] . ";" . $_POST["biologico"] . ";" . $_POST["primera"] . ";" . $_POST["segunda"];
                        $archivo = fopen("vacunas.txt", "w");
                        fwrite($archivo, "$contenido");
                        fclose($archivo);
             
                    }  

                }

                imprimir();

           
                   
               

                    
                    // if (isset($_POST['identificacion'])) {
                    //     $contenido = $_POST["identificacion"] . ";" . $_POST["nombres"] . ";" . $_POST["apellidos"] . ";" . $_POST["biologico"] . ";" . $_POST["primera"] . ";" . $_POST["segunda"];
  
                    //     if (file_exists("vacunas.txt")) {
                    //         $archivo = fopen("vacunas.txt", "a");
                    //         fwrite($archivo, PHP_EOL . "$contenido");
                    //         fclose($archivo);
                    //     } else {
                    //         $archivo = fopen("vacunas.txt", "w");
                    //         fwrite($archivo, "$contenido");
                    //         fclose($archivo);
                    //     }                
                    //     $archivo = fopen("vacunas.txt", "r");
                    //     while (!feof($archivo)) {
                    //         $linea = fgets($archivo);
                    //         //  echo $linea."<br/>";     
                    //         $vacunados = explode(";", $linea);
                    //         echo " <tr>
                    //                         <th scope='row'>$vacunados[0]</th>
                    //                         <td>$vacunados[1]</td>
                    //                         <td>$vacunados[2]</td>
                    //                         <td>$vacunados[3]</td>
                    //                         <td>$vacunados[4]</td>
                    //                         <td>$vacunados[5]</td>
                    //                 </tr>";

                    //     }
                    // }else{
                    //     echo 'hola2';
                    // }


                ?>
            </tbody>
        </table>
    </div>


</body>

</html>